﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBall : MonoBehaviour
{
    public float Timer;
    float defaultTimer = 5;

    public Transform TeleportPoint;
    public Transform Ball;

    private void OnTriggerStay(Collider other)
    {
        Timer -= Time.deltaTime;
        if (Timer <= 0)
        {
            Ball.SetPositionAndRotation(TeleportPoint.position, TeleportPoint.rotation);
            Timer = defaultTimer;
        }
    }
}
