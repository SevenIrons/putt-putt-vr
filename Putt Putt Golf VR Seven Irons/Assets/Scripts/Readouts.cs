﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Readouts : MonoBehaviour
{
    public Text X;
    public Text Y;
    public Text Z;
    public Text Fps;
    float deltaTime = 0;
    float fps = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FPS();
        X.text = transform.rotation.eulerAngles.x.ToString();
       Y.text = transform.rotation.eulerAngles.y.ToString();
       Z.text = transform.rotation.eulerAngles.z.ToString();

    }

    void FPS()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;
        Fps.text = fps.ToString();
    }
}
