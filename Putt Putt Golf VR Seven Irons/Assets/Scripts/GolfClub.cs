﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolfClub : MonoBehaviour
{
    protected Rigidbody GolfClubRigidBody = null;

    protected GolfBehaviour CurrentBehaviour = null;

    public List<GolfBehaviour> behaviours;

    public static GolfBall golfBall = null;

    // Start is called before the first frame update
    void Start()
    {
        GolfClubRigidBody = GetComponent<Rigidbody>();

        golfBall = FindObjectOfType<GolfBall>();

        if (behaviours.Count > 0)
        {
            CurrentBehaviour = behaviours[0];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeBehaviour(int index)
    {
        if(index < behaviours.Count)
        {
            CurrentBehaviour = behaviours[index];
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == golfBall.gameObject)
        {
            if(golfBall.CurrentBehaviour)
            {
                golfBall.CurrentBehaviour.ExitBehaviour(golfBall);
            }

            if(CurrentBehaviour)
            {
                golfBall.CurrentBehaviour = CurrentBehaviour;

                golfBall.CurrentBehaviour.OnClubHit(this, golfBall);
            }


        }
    }
}
