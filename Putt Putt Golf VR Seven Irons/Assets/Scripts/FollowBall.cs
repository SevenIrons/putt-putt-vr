﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBall : MonoBehaviour
{
    public GameObject followObject;
    public int height = 0;
    private Vector3 pos = Vector3.zero;
    public bool ignoreY = true;

    // Update is called once per frame
    void Update()
    {
        pos = followObject.transform.position;

        if(ignoreY)
        {
            pos.y = height;
        }

        gameObject.transform.position = pos;
    }
}
