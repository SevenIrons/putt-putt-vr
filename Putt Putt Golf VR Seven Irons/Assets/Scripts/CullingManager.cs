﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CullingManager : MonoBehaviour
{

    public List<MeshRenderer> renderers = new List<MeshRenderer>();
    //public List<GameObject> blacklist = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        FindAllMeshesInScene();

        ApplyCulling();
        OVRManager.display.displayFrequency = 72;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ApplyCulling()
    {
        foreach (var rend in renderers)
        {
            rend.gameObject.AddComponent<Culling>(); //add culling script to it
            rend.GetComponent<Culling>().m_renderer = rend; //set its reference to its renderer
            rend.GetComponent<Culling>().cam = GetComponent<Camera>(); //set its reference to the main camera.
        }
    }

    void FindAllMeshesInScene()
    {
        GameObject[] objectInScene = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[]; //Find every game object in scene

        for (int i = 0; i < objectInScene.Length; i++)
        {
            if (objectInScene[i].scene != null) //if the object is in scene
            {
                if (objectInScene[i].GetComponent<MeshRenderer>() != null) //if it has a mesh collider
                {
                    if (objectInScene[i].hideFlags == HideFlags.None) //it doesnt have any hide flags
                    {
                        renderers.Add(objectInScene[i].GetComponent<MeshRenderer>()); //add to mesh renderer list
                    }
                }
            }
        }
    }


}
