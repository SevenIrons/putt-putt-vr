﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ClubPhysics : MonoBehaviour
{
    [Range(0,1000)]
    public float scalar = 0;
    public bool isTouching = false;
    private Vector3 previousPos;
    private Vector3 currentPos;
    private Vector3 velocity;
    public float vel = 0;

    public BallPhysics ball = null;

    private AudioSource audioSource;

    private float collisionTimer = 0;
    private bool colliderdisabled = false;


  //  public TextMeshPro forceeOutPutLog;
    public Collider triggerField;
    public Collider ballCol;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if(!ball)
        {
            ball = FindObjectOfType<BallPhysics>();
        }
    }

    private void FixedUpdate()
    {
        currentPos = transform.position;
        velocity = (currentPos - previousPos) / Time.fixedDeltaTime;
        vel = velocity.magnitude;
        previousPos = currentPos;

        if (colliderdisabled)
        {
            if (collisionTimer > 1)
            {
                colliderdisabled = false;
                Physics.IgnoreCollision(triggerField, ballCol, false);
                collisionTimer = 0;
            }
            else
            {
                collisionTimer += Time.deltaTime;
            }
        }


    }


    private void OnCollisionEnter(Collision other)
    {
        if(!ball.moving)
        {
            if (other.transform.tag == "Ball")
            {
                isTouching = true;
                Vector3 outPutForce = velocity * scalar;
                //outPutForce.x = Mathf.Clamp(outPutForce.x, -200, 200);
                //outPutForce.y = Mathf.Clamp(outPutForce.y, -200, 200);
                //outPutForce.z = Mathf.Clamp(outPutForce.z, -200, 200);
                ball.m_rigidBody.AddForce(velocity, ForceMode.Impulse);

                //    forceeOutPutLog.text += "{" + outPutForce + "},";
            }
        }
        Physics.IgnoreCollision(triggerField, ballCol, true);
        colliderdisabled = true;
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.transform.tag == "Ball")
    //    {
    //        isTouching = true;
    //        Vector3 outPutForce = velocity * scalar;
    //        outPutForce.x = Mathf.Clamp(outPutForce.x, -200, 200);
    //        outPutForce.y = Mathf.Clamp(outPutForce.y, -200, 200);
    //        outPutForce.z = Mathf.Clamp(outPutForce.z, -200, 200);
    //        ball.m_rigidBody.AddForce(outPutForce, ForceMode.Acceleration);
    //    }
    //}

    private void OnCollisionExit(Collision other)
    {
        if(!ball.moving)
        {
            if (other.transform.tag == "Ball")
            {
                ScoreManager.manager.AddHit();
                ball.moving = true;
                audioSource.Play();
            }
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.transform.tag == "Ball")
    //    {
    //        isTouching = true;
    //        Rigidbody ballRigidBody = collision.transform.GetComponent<Rigidbody>();

    //        ballRigidBody.AddForce(velocity * vel, ForceMode.Acceleration);

    //    }
    //}
    //private void OnCollisionExit(Collision collision)
    //{
    //    if (collision.transform.tag == "Ball")
    //    {
    //        isTouching = false;
    //    }
    //}
}
