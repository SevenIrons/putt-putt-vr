﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HapticFeedback : MonoBehaviour
{
       public OVRPlugin.HapticsState a;
    public List<Text> Showcase = new List<Text>();

    public float value;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.SetControllerVibration(value,value, OVRInput.Controller.RTouch);
        for (int i = 0; i < Showcase.Count; i++)
        {
            Showcase[i].text = value.ToString();
        }
    }

    public void Up()
    {
        value += 1;
    }

    public void Down()
    {
        value += 1;
    }
}
