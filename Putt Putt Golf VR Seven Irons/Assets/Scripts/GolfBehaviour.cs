﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GolfBehaviour", menuName = "GolfBehaviour")]
public class GolfBehaviour : ScriptableObject
{
    public float extraForce = 1.0f;

    //activates special ability when interacting with obstacle if any
    public virtual void OnObstacleHit(GameObject obstacle, GolfBall ball)
    {
        ball.DebugMessage("OnObstacleHit");
    }

    //effect on club hit
    public virtual void OnClubHit(GolfClub club, GolfBall ball)
    {
        ball.DebugMessage("OnClubHit");
        ScoreManager.manager.AddHit();
    }

    //cleans up any changes done by behaviour
    public virtual void ExitBehaviour(GolfBall ball)
    {
        ball.DebugMessage("ExitBehaviour");
    }


}
