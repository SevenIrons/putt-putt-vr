﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class Winstate : MonoBehaviour
{
    public ParticleSystem winParticle;
    public AudioSource winAudio;
    public bool testBool = false;
    public AnimationCurve scaleCurve;
    public AnimationCurve positionCurve;
    public float Modifier;

    public float yPosOfCanvas = 1.0f;

    //public AnimationCurve myOtherCurve;

    public Vector3 StartSize = Vector3.zero;
    public Vector3 EndSize = Vector3.zero;
    public Canvas winCanvas;
    public Vector3 endPos = Vector3.zero;


    public Vector3 winOffsetTeleport = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {

        //winParticle.aw = false;
        //winParticle.loop = false;

        //winAudio = GetComponent<AudioSource>();
        winAudio.playOnAwake = false;
        winAudio.loop = false;
        winCanvas.gameObject.SetActive(false);

        //determine our canvas tween values:
        StartSize = winCanvas.transform.localScale / 2;
        EndSize = winCanvas.transform.localScale * Modifier;

        if (testBool == true)
        {
            ActivateTweening();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball")
        {
            winCanvas.gameObject.SetActive(true);
            ScoreManager.manager.AddHole();

            if(winParticle)
            {
                winParticle.Play();
                winAudio.Play();
            }

            //tween our gutter in:
            ActivateTweening();

            Teleport teleport = FindObjectOfType<Teleport>();

            if(teleport)
            {
              //  winCanvas.transform.LookAt(teleport.player);
                teleport.SetTeleportPoint(winOffsetTeleport + gameObject.transform.position);
            }
        }
    }


    public void ActivateTweening()
    {
        Tween.LocalScale(winCanvas.transform, StartSize, EndSize, 1f, 0, scaleCurve);
        Tween.LocalPosition(winCanvas.transform, endPos, 1f, 0, positionCurve);
        ScoreManager.manager.GolfPar();
    }
}
