﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clubChanger : MonoBehaviour
{

    public List<GameObject> clubObjects = new List<GameObject>();
    public List<GameObject> clubDisplayObjects = new List<GameObject>();
    public int iterator;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Down();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Up();
        }





    }

    public void Up()
    {
        clubObjects[iterator].SetActive(false);
        clubDisplayObjects[iterator].SetActive(false);
        iterator++;
        if (iterator >= clubObjects.Count)
        {
            iterator = 0;
        }
        clubDisplayObjects[iterator].SetActive(true);
        clubObjects[iterator].SetActive(true);
    }

    public void Down()
    {
        clubObjects[iterator].SetActive(false);
        clubDisplayObjects[iterator].SetActive(false);
        iterator--;
        if (iterator < 0)
        {
            iterator = clubObjects.Count - 1;
        }
        clubDisplayObjects[iterator].SetActive(true);
        clubObjects[iterator].SetActive(true);
    }


   
}
