﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Teleport : MonoBehaviour
{
    public Transform player;
    public Transform teleportPoint;
    public Transform teleportPos;
    private Rigidbody ballRigidBody;
    private SpriteRenderer arrow;
    public float sensitivity = 0;
    public TextMeshPro controllerOutput;
    public TextMeshPro touchDeltaOutput;
    public float touchDeltaY = 0;
    public float touchDeltaX = 0;
    public float fadeTime = 0;
    public float fadeAmount = 0;
    private bool teleport = false;
    private bool fadeIn = false;
    private bool fadeOut = false;
    private float offset = 0;
    public Image fadeImage;
    public BallPhysics ballPhysiics;

    public GameObject winStateMenu;
    public GameObject menu;

    // Start is called before the first frame update
    void Start()
    {
        if (teleportPoint.parent != null)
        {
            if (teleportPoint.parent.tag == "Ball")
            {
                ballRigidBody = teleportPoint.parent.GetComponent<Rigidbody>();
            }
        }

        if (teleportPoint.GetComponentInChildren<SpriteRenderer>() != null)
        {
            arrow = teleportPoint.GetComponentInChildren<SpriteRenderer>();
            arrow.enabled = false;
        }

        offset = Vector3.Distance(player.position, teleportPoint.position);

        player.transform.position = teleportPos.position;

    }

    // Update is called once per frame
    void Update()
    {

        if (ballRigidBody.velocity.magnitude == 0)
        {
            arrow.enabled = true;
            float inputDelta = 0; //the scroll from the mouse or the touch pad
                                  //for testing on pc
                                  //for the actual
#if UNITY_ANDROID
            if (controllerOutput != null)
            {
                //OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;     
                controllerOutput.text = "" + OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.Active);
            }

            float newDeltaY = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.Active).y;
            float newDeltaX = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.Active).x;

            if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad))
            {
                //player.transform.position = teleportPoint.position;
                //player.transform.position += new Vector3(0, offset, 0);

                //  touchDeltaY = Mathf.Clamp(touchDeltaY, -1, 1);
                touchDeltaX += newDeltaX;
                inputDelta += touchDeltaX * sensitivity;

                //if (newDeltaY > 0.1f)
                //{
                //    if (newDeltaY > touchDeltaY)
                //    {
                //        touchDeltaY = newDeltaY;
                //        inputDelta += Mathf.Abs(touchDeltaY) * sensitivity;
                //    }
                //}
                //if (newDeltaY < -0.1f)
                //{
                //    if (newDeltaY < touchDeltaY)
                //    {
                //        touchDeltaY = newDeltaY;

                //        inputDelta -= Mathf.Abs(touchDeltaY) * sensitivity;
                //    }
                //}

            }
            else
            {
                touchDeltaX = 0;
            }
            if (touchDeltaOutput != null)
            {
                touchDeltaOutput.text = "" + touchDeltaY;
            }

#endif

#if UNITY_EDITOR
            inputDelta = Input.mouseScrollDelta.y * sensitivity;
#endif
            if ((Input.GetKeyDown(KeyCode.Q) || OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger)))
            {
                if (!menu.activeInHierarchy && !winStateMenu.activeInHierarchy)
                {
                    teleport = true;
                    fadeImage.transform.parent.gameObject.SetActive(true);
                } 
            }
            teleportPoint.RotateAround(teleportPoint.transform.parent.position, teleportPoint.up, inputDelta);
        }
        else
        {
            arrow.enabled = false;
            //  ballPhysiics.StopBall();
        }


        if (teleport)
        {
            if (fadeImage.color.a <= 0)
            {
                if (!fadeOut)
                {
                    fadeIn = true;
                }
                else
                {
                    fadeImage.transform.parent.gameObject.SetActive(false);
                    teleport = false;
                    fadeOut = false;
                }
            }
            if (fadeImage.color.a >= fadeTime)
            {
                //player.transform.position = teleportPoint.position;
                player.transform.position = teleportPos.position;
#if UNITY_EDITOR
                player.transform.LookAt(ballRigidBody.transform.position);
#endif
                fadeIn = false;
                fadeOut = true;
            }
            if (fadeOut)
            {
                fadeImage.color -= new Color(0, 0, 0, fadeAmount * Time.deltaTime);
            }
            if (fadeIn)
            {
                fadeImage.color += new Color(0, 0, 0, fadeAmount * Time.deltaTime);
            }
        }
    }

    public void SetTeleportPoint(Vector3 newPoint)
    {
        teleportPos.parent = null;
        teleportPos.position = newPoint;
    }
}
