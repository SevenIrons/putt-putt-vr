﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterSound : MonoBehaviour
{
    protected AudioSource source = null;


    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball")
        {
            if (source)
            {
                source.PlayOneShot(source.clip);
            }
        }
    }
}
