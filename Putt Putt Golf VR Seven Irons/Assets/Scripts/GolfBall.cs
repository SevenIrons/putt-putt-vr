﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GolfBall : MonoBehaviour
{
    public Rigidbody GolfClubRigidBody = null;

    public GolfBehaviour CurrentBehaviour = null;

    public Text DebugText = null;
    
    // Start is called before the first frame update
    void Start()
    {
        GolfClubRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "InteractableObstacle")
        {
            if(CurrentBehaviour)
            {
                CurrentBehaviour.OnObstacleHit(other.gameObject, this);
            }
        }
    }

    public void DebugMessage(string text)
    {
        if(DebugText)
        {
            DebugText.text = text;
        }

    }
}
