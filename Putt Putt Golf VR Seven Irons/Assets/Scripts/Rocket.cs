﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{

    public float timer = 2.0f;
    public float secondTimer = 1.0f;

    protected float currentTime = 0.0f;


    public GameObject rocketTrailPrefab = null;
    public GameObject rocketExplosion = null;

    protected bool destroySelf = false;
    protected bool activated = false;

    public Vector3 translate;

    // Update is called once per frame
    void Update()
    {
        if(destroySelf)
        {
            currentTime += Time.deltaTime;

            if (currentTime > timer)
            {
                if (rocketExplosion)
                {
                    Vector3 particleSpawnPos = gameObject.transform.position;
                    Instantiate(rocketExplosion, particleSpawnPos, Quaternion.identity);
                }


                gameObject.SetActive(false);
            }
        }


        if(activated)
        {
            currentTime += Time.deltaTime;

            if (currentTime > timer)
            {

                destroySelf = true;
                activated = false;

                timer = secondTimer;
                currentTime = 0.0f;
            }
            else
            {
                gameObject.transform.Translate(translate * Time.deltaTime);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball" || other.tag == "Player" || other.tag == "GolfClub")
        {
            if(rocketTrailPrefab)
            {
                Instantiate(rocketTrailPrefab, gameObject.transform);
            }

            activated = true;
        }
    }
}
