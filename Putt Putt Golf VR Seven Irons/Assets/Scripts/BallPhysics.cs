﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPhysics : MonoBehaviour
{
    public Rigidbody m_rigidBody;
    //[HideInInspector]
    public bool moving = false;
    public Vector3 respawnPos;
    public ClubPhysics clubPhysics;
    public Teleport clubTeleport;
    public float ballSpeed = 0;

    private AudioSource audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody>();
        respawnPos = transform.position;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ballSpeed = m_rigidBody.velocity.magnitude;

        //if (ballSpeed > 0.05f)
        //{
        //    moving = true;
        //}


        if (moving)
        {
            // m_rigidBody.velocity *=  -0.01f * Time.deltaTime;
            if (ballSpeed <= 0.05f)
            {
                Debug.Log("Not Moving");
                StopBall();
            }
        }
    }


    public void StopBall()
    {
        m_rigidBody.isKinematic = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        respawnPos = transform.position;
        m_rigidBody.isKinematic = false;
        moving = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        audioSource.Play();
        if (collision.transform.tag == "OutOfBounds")
        {
            transform.position = respawnPos;
            StopBall();
            
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (moving)
        {
            if (ballSpeed >= 0 && ballSpeed <= 0.5f)
            {
                m_rigidBody.velocity -= m_rigidBody.velocity * 0.6f * Time.fixedDeltaTime;
                ballSpeed = m_rigidBody.velocity.magnitude;
            }
        }
      


    }


}
