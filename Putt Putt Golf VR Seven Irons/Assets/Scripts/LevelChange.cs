﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[Serializable]
public class SceneNames
{
    public string LevelName = "Some level";
    public int index = 0;
    public string Description = "Some Description";
}

public class LevelChange : MonoBehaviour
{
    public static int SceneIndex = 0;

    public static bool SceneRedirect = false;

    protected int LoadingSceneIndex = 1;

    protected static float waitTime = 5.0f;

    public List<SceneNames> sceneNames;

    public LO_LoadingScreen screen;

    public GameObject button = null;

    public bool AutomaticTransition = false;

    public void Start()
    {
        //if we are in the loading screen
        if(SceneRedirect)
        {
            if(button)
            {
                button.SetActive(false);
            }

            screen = FindObjectOfType<LO_LoadingScreen>();

            for (int i=0; i< sceneNames.Count; i++)
            {
                if(sceneNames[i].index == SceneIndex)
                {
                    screen.titleText = sceneNames[i].LevelName;
                    screen.titleDescText = sceneNames[i].Description;
                    screen.ChangeTitle(sceneNames[i].LevelName, sceneNames[i].Description);
                    break;
                }
            }

            SceneRedirect = false;
            StartCoroutine(WaitForNextScene());
        }
    }

    //change the level by index
    public void ChangeLevel(int index)
    {
        if (index < SceneManager.sceneCountInBuildSettings)
        {
            SceneIndex = index;
            SceneRedirect = true;
            SceneManager.LoadScene(LoadingSceneIndex);
        }

    }

    //wait to move to next scene
    protected IEnumerator WaitForNextScene()
    {
        yield return new WaitForSecondsRealtime(waitTime);

        if(AutomaticTransition)
        {
            SceneManager.LoadScene(SceneIndex);
        }
        else
        {
            if(button)
            {
                button.SetActive(true);
            }
        }

    }

    //restart the current level
    public void RestartLevel()
    {
        int level = SceneManager.GetActiveScene().buildIndex;

        ChangeLevel(level);
    }

    public void MoveToNextLevel()
    {
        SceneManager.LoadScene(SceneIndex);
    }

}
