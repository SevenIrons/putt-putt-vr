﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeActiveMenu : MonoBehaviour
{
    public GameObject Menus;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //OVRInput.Update();
        if (OVRInput.GetUp(OVRInput.RawButton.Back) == true && Menus.gameObject.activeInHierarchy == true)     
        {
            Menus.gameObject.SetActive(false);
        }
        else if (OVRInput.GetUp(OVRInput.RawButton.Back) == true && Menus.gameObject.activeInHierarchy == false)
        {
            Menus.gameObject.SetActive(true);
        }
    }
}
