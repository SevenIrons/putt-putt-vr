﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{

    public List<Text> Showcase = new List<Text>();
    public bool refreshButton;
    public bool showingOptions = false;
    public GameObject refreshRateMenu;
    public GameObject ffrMenu;
    public GameObject clubChanger;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(refreshButton == true)
        {
            for (int i = 0; i < Showcase.Count; i++)
            {
                Showcase[i].text = OVRManager.display.displayFrequency.ToString();
            }
        }
        else
        {
            for (int i = 0; i < Showcase.Count; i++)
            {
                Showcase[i].text = OVRManager.tiledMultiResLevel.ToString();
            }
        }
    }


    public void ChangeRefreshRate(float refreshRate)
    {
        OVRManager.display.displayFrequency = refreshRate;
    }

    public void ChangeFFR(int rate)
    {
        OVRManager.tiledMultiResLevel = (OVRManager.TiledMultiResLevel)rate; 
    }

    public void ResetScore()
    {
        
    }

    public void ResetGame(Canvas canvas)
    {
        canvas.gameObject.SetActive(true);
    }

    public void TurnMeOff()
    {
        gameObject.SetActive(false);
    }

    public void ShowMenu()
    {
        if (showingOptions == false)
        {
            ffrMenu.SetActive(false);
            refreshRateMenu.SetActive(false);
            clubChanger.SetActive(true);
        }
        else
        {
            ffrMenu.SetActive(true);
            refreshRateMenu.SetActive(true);
            clubChanger.SetActive(false);
        }
    }


}
