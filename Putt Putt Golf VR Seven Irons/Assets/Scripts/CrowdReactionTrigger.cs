﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdReactionTrigger : MonoBehaviour
{
    private AudioSource audioSource;
    
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Ball")
        {
            audioSource.Play();
        }
    }
}
