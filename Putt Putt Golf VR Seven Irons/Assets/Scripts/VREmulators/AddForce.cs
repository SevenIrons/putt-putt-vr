﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour
{

    public Rigidbody m_Rigidbody;
    public float forceAmount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if  (Input.GetKeyDown(KeyCode.R))
        {
              m_Rigidbody.AddForce(-transform.right * forceAmount, ForceMode.Impulse);
        }
    }
}
