﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandEmulator : MonoBehaviour
{
    Vector2 lastMouse;
    public float sensitivity = 100;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if(Input.GetMouseButton(1))
        {
            Vector3 rotation = new Vector3(Input.GetAxis("Mouse Y") * sensitivity, Input.GetAxis("Mouse X") * sensitivity, 0);

            transform.Rotate(rotation);
            Debug.Log(transform.rotation.eulerAngles);
        }
#endif
    }

    #region Input Emulation

    public Vector2 Get(OVRInput.Axis2D axis)
    {
#if UNITY_EDITOR
        if(axis == OVRInput.Axis2D.PrimaryTouchpad)
            return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
#endif
        return OVRInput.Get(axis);
    }

    public bool Get(OVRInput.Button button)
    {

#if UNITY_EDITOR 
        switch(button)
        {
            case OVRInput.Button.PrimaryIndexTrigger:
                return Input.GetMouseButton(0);
            case OVRInput.Button.PrimaryTouchpad:
                return Input.GetMouseButton(1);
            case OVRInput.Button.Back:
                return Input.GetMouseButton(2);
        }
#endif


        return OVRInput.Get(button);
    }

    public bool GetDown(OVRInput.Button button)
    {

#if UNITY_EDITOR 
        switch (button)
        {
            case OVRInput.Button.PrimaryIndexTrigger:
                return Input.GetMouseButtonDown(0);
            case OVRInput.Button.PrimaryTouchpad:
                return Input.GetMouseButtonDown(1);
            case OVRInput.Button.Back:
                return Input.GetMouseButtonDown(2);
        }
#endif


        return OVRInput.GetDown(button);
    }

    public bool GetUp(OVRInput.Button button)
    {

#if UNITY_EDITOR 
        switch (button)
        {
            case OVRInput.Button.PrimaryIndexTrigger:
                return Input.GetMouseButtonUp(0);
            case OVRInput.Button.PrimaryTouchpad:
                return Input.GetMouseButtonUp(1);
            case OVRInput.Button.Back:
                return Input.GetMouseButtonUp(2);
        }
#endif


        return OVRInput.GetUp(button);
    }
    #endregion
}
