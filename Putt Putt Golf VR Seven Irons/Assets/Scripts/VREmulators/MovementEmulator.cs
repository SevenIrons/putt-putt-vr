﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementEmulator : MonoBehaviour
{
    public float movementSpeed = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if testing the project in the editor on PC
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * movementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += transform.forward * -movementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += transform.right * -movementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * movementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += new Vector3(0,movementSpeed * Time.deltaTime,0);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position -= new Vector3(0, movementSpeed * Time.deltaTime, 0);
        }

#endif


    }
}
