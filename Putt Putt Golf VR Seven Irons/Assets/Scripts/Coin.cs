﻿using UnityEngine;


public class Coin : MonoBehaviour
{
    [Tooltip("Amount of score of the coin")]
    public int ScoreAmount = 1;

    public Vector3 rotation;

    public float timer = 1.0f;

    public GameObject ParticlePrefab = null;

    public Vector3 particleOffset = Vector3.zero;

    protected float currentTime = 0.0f;

    protected bool destroySelf = false;

    private void OnTriggerEnter(Collider other)
    {
        //if golfball intersects with the coin, add score and create particle system
        if (other.tag == "Ball")
        {
            ScoreManager.manager.AddScore(ScoreAmount);

            if (ParticlePrefab)
            {
                Vector3 particleSpawnPos = gameObject.transform.position + particleOffset;
                Instantiate(ParticlePrefab, particleSpawnPos, Quaternion.identity);
            }

            gameObject.SetActive(false);
        }
    }

    public void FixedUpdate()
    {
        transform.Rotate(rotation);
    }
}
