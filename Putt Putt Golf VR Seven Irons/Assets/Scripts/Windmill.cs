﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windmill : MonoBehaviour
{
    //rotation settings
    public float NormalRotation = 360.0f;
    public float FastRotation = 720.0f;
    public float FastTime = 1.0f;

    //objects
    public GameObject WindMillBlades = null;
    public ParticleSystem WindMillParticle = null;

    protected bool fastRotationMode = false;
    protected Vector3 CurrentRotation;
    protected float DecreaseAmount = 0;

    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        //blades rotate in the x direction
        CurrentRotation.x = NormalRotation;

        //check fast time and rotations are valid
        if(FastTime <= 0)
        {
            FastTime = 1.0f;
            Debug.Log("timer is too small!");
        }

        if(NormalRotation >= FastRotation)
        {
            float temp = NormalRotation;

            NormalRotation = FastRotation;
            FastRotation = NormalRotation + 0.1f;

            Debug.Log("Fast rotation the same or less than Normal rotation!");
        }
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(fastRotationMode)
        {
            //decrease speed until normal speed is reached
            if (CurrentRotation.x > NormalRotation)
            {
                CurrentRotation.x -= DecreaseAmount*Time.deltaTime;
            }
            else
            {
                CurrentRotation.x = NormalRotation;
                fastRotationMode = false;

                if(WindMillParticle)
                {
                    WindMillParticle.Play(false);
                }
            }
        }

        WindMillBlades.transform.Rotate(CurrentRotation * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" || other.tag == "GolfClub")
        {
            audioSource.Play();
            fastRotationMode = true;
            CurrentRotation.x = FastRotation;

            //how much rotation per second needed to reach the normal speed in 'FastTime' seconds
            DecreaseAmount = (FastRotation - NormalRotation) / FastTime;

            if (WindMillParticle)
            {
                WindMillParticle.Play(true);
            }

        }
    }
}
