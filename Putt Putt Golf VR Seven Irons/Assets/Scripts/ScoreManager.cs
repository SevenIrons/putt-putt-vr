﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System;
using UnityEngine.UI;
using System.Text;

[Serializable]
public class ScoreNamePair
{
    public int value;
    public string name;
}

[Serializable]
public class HighScoreSave
{
    public List<ScoreNamePair> highScore;
}

public class ScoreManager : MonoBehaviour
{
    //singleton scoremanager
    public static ScoreManager manager = null;

    [Header("Score variables")]
    public int hits = 0;
    public int hitsMultiplier = 100;
    public int MaxHits = 10;
    public int ScoreHitAmount = 0;
    public int pickupScore = 0;
    protected static int scoreFromOtherHoles = 0;
    protected static int Holes = 0;

    [Header("Textvalues")]
    public Text scoreText = null;
    public Text StrokesText = null;
    public Text HolesText = null;
    public List<Text> highScoreTexts;

    public InputName inputName = null;

    public Text ScoreType = null;

    public bool Testing = false;

    public ScoreNamePair scoreName = null;
    public static HighScoreSave save = null;

    protected LevelChange LevelChange = null;



    // Start is called before the first frame update
    void Awake()
    {
        //singleton pattern
        if (!manager)
        {
            manager = this;
        }
        else
        {
            Destroy(this);
            Debug.Log("An instance of scoremanager already exists");
        }

        LevelChange = FindObjectOfType<LevelChange>();


        scoreName = new ScoreNamePair
        {
            name = "nan",
            value = 0
        };


        if (save == null)
        {
            LoadHighScores();
        }

        if (inputName && !Testing)
        {
            inputName.gameObject.SetActive(false);
        }

        DisplayHighScores();

        ScoreHitAmount = (MaxHits - hits) * hitsMultiplier;

    }

    // Update is called once per frame
    void Update()
    {

        scoreName.value = (pickupScore + scoreFromOtherHoles + ScoreHitAmount);

        if (scoreText)
        {
            scoreText.text = "" + scoreName.value;
        }

        if (StrokesText)
        {
            StrokesText.text = "" + hits;
        }

        if (HolesText)
        {
            HolesText.text = "" + Holes;
        }
    }

    //add an extra hit - lowers score
    public void AddHit()
    {
        hits++;
        ScoreHitAmount = ((MaxHits - hits) + 1) * hitsMultiplier;

        if(ScoreHitAmount < 0)
        {
            ScoreHitAmount = 0;
        }
    }

    //adds extra score
    public void AddScore(int value)
    {
        if (value > 0)
        {
            pickupScore += value;
        }
    }

    //load highscore from a save file (stored in player prefs)
    protected void LoadHighScores()
    {
        bool fail = true;

        string savefile = "";

        if (PlayerPrefs.HasKey("highscore"))
        {
            savefile = PlayerPrefs.GetString("highscore");
        }

        Debug.Log(savefile);

        //if savefile isn't empty try to deserialise
        if (savefile.Length > 0)
        {
            TextReader reader = new StringReader(savefile);


            XmlSerializer formatter = new XmlSerializer(typeof(HighScoreSave));

            try
            {
                save = (HighScoreSave)formatter.Deserialize(reader);
                fail = false;
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                save = new HighScoreSave();
                Debug.Log("Cannot Read SaveFile");
                fail = true;
            }
            finally
            {

            }


            reader.Close();
        }


        if (fail)
        {
            ResetHighScoreTable();
        }
    }

    //save the highscores
    protected void SaveHighScores()
    {
        StringWriter stream = new StringWriter();

        XmlSerializer formatter = new XmlSerializer(typeof(HighScoreSave));

        formatter.Serialize(stream, save);

        string file = stream.ToString();

        PlayerPrefs.SetString("highscore", file);
        stream.Close();
    }

    //check the highscores
    public void CheckHighScore()
    {
        bool ScoreChanged = false;

        for (int i = 0; i < save.highScore.Count; i++)
        {
            if (save.highScore[i].value <= scoreName.value)
            {
                save.highScore[save.highScore.Count - 1] = scoreName;
                ScoreChanged = true;
                break;
            }
        }

        if (ScoreChanged)
        {
            save.highScore.Sort((a, b) => b.value.CompareTo(a.value));
            SaveHighScores();
            DisplayHighScores();
        }

    }

    //change the name as a string
    public void ChangeName(string newName)
    {
        scoreName.name = newName;

        CheckHighScore();

        if (inputName)
        {
            inputName.gameObject.SetActive(false);
        }

        LevelChange.ChangeLevel(0);
    }

    //show the highscores
    public void DisplayHighScores()
    {
        for (int i = 0; i < highScoreTexts.Count; i++)
        {
            if (i < save.highScore.Count)
            {
                highScoreTexts[i].text = save.highScore[i].name + "             " + save.highScore[i].value;
            }
            else
            {
                break;
            }
        }
    }

    //add hole - counts as finishing a level as well
    public void AddHole()
    {
        Holes++;
        scoreName.value = pickupScore + scoreFromOtherHoles + ScoreHitAmount;
        scoreFromOtherHoles += scoreName.value;
        scoreName.value = 0;
    }

    //show the input name canvas
    public void ShowInputName()
    {
        if (inputName)
        {
            inputName.gameObject.SetActive(true);
        }
    }

    //resets the highscore table
    public void ResetHighScoreTable()
    {
        scoreName.value = 0;
        scoreName.name = "Nan";

        if(save == null)
        {
            save = new HighScoreSave()
            {
                highScore = new List<ScoreNamePair>()
            };
        }

        for (int i = 0; i < 10; i++)
        {
            save.highScore.Add(scoreName);
        }

        SaveHighScores();
    }


    public void GolfPar()
    {
        if(ScoreType && hits > -1)
        {
            string scoreDescription = "SomeScore";

            switch (hits)
            {
                case 0:
                    {
                        scoreDescription = "Telekinesis";
                        break;
                    }

                case 1:
                    {

                        scoreDescription = "Hole in One"; 
                        break;
                    }
                case 2:
                    {
                        scoreDescription = "Birdy";
                        break;
                    }
                case 3:
                    {
                        scoreDescription = "Par";
                        break;
                    }
                case 4:
                    {
                        scoreDescription = "Bogey";
                        break;
                    }
                case 5:
                    {
                        scoreDescription = "Double Bogey";
                        break;
                    }
                case 6:
                    {
                        scoreDescription = "Triple Bogey";
                        break;
                    }
 
                default:
                    {
                        scoreDescription = (hits - 3) +  " Over Par";
                        break;
                    }
            }

            ScoreType.text = scoreDescription;
        }
    }


 

    

}

