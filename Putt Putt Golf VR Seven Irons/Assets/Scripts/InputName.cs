﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputName : MonoBehaviour
{
    private char[] CurrentName;

    private bool recievingInput = false;

    private int CurrentCharacter = -1;

    public List<Text> texts;

    public Color SelectedColor = Color.red;
    private Color OriginalColor = Color.black;


    private float inputDelta = 0.0f;
    private float touchDeltaY = 0.0f;


    private readonly float timer = 0.3f;

    private float currentTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        CurrentName = new char[texts.Count];

        for(int i=0; i< texts.Count; i++)
        {
            if(texts[i].text.Length > 0)
            {
                CurrentName[i] = texts[i].text[0];
            }
        }

        //recievingInput = true;
        //CurrentCharacter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if (recievingInput)
        {
            #if UNITY_ANDROID && !UNITY_EDITOR

                        float newDeltaY = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.Active).y;

                        if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad))
                        {
                            touchDeltaY = Mathf.Clamp(touchDeltaY, -1, 1);

                            if (newDeltaY > 0.6f)
                            {
                                if (newDeltaY > touchDeltaY)
                                {
                                    touchDeltaY = newDeltaY;
                                    inputDelta += Mathf.Abs(touchDeltaY);
                                    TouchInput(true);

                                }
                            }
                            if (newDeltaY < -0.6f)
                            {
                                if (newDeltaY < touchDeltaY)
                                {
                                    touchDeltaY = newDeltaY;
                                    inputDelta -= Mathf.Abs(touchDeltaY);
                                    TouchInput(false);
                                }
                            }

                        }
                        else
                        {
                            touchDeltaY = 0;
                        }

            #endif

            #if UNITY_EDITOR
            if (Input.mouseScrollDelta.y > 0.8)
                {
                    TouchInput(true);
                }
                else if (Input.mouseScrollDelta.y < -0.8)
                {
                    TouchInput(false);
                }
            #endif
        }
    }

    //change a letter in the name
    private void ChangeLetter(int letterindex, int increase, bool positive)
    {
        if (letterindex < CurrentName.Length)
        {
            int temp = CurrentName[letterindex];

            if (positive)
            {
                temp += increase;
            }
            else
            {
                temp -= increase;
            }

            if (temp > 122)
            {
                temp = 97;
            }

            if (temp < 97)
            {
                temp = 122;
            }

            CurrentName[letterindex] = (char)temp;

            texts[letterindex].text = CurrentName[letterindex] + "";
        }
    }

    //increase a letter
    public void AddToLetter(int letterindex)
    {
        if (letterindex < CurrentName.Length)
        {
            ChangeLetter(letterindex, 1, true);
        }
    }

    //subtract a letter
    public void SubtractToLetter(int letterindex)
    {
        if (letterindex < CurrentName.Length)
        {
            ChangeLetter(letterindex, 1, false);
        }
    }

    //use input to scroll name
    public void TouchInput(bool positive)
    {
        if (recievingInput && CurrentCharacter > -1)
        {
            ChangeLetter(CurrentCharacter, 1, true);
        }
    }

    //change input
    public void SetInput(int letterindex)
    {
        if (letterindex < CurrentName.Length)
        {
            if(currentTime > timer)
            {
                currentTime = 0.0f;

                if (letterindex == CurrentCharacter)
                {
                    recievingInput = false;
                    texts[CurrentCharacter].color = OriginalColor;
                    CurrentCharacter = -1;
                }
                else
                {
                    recievingInput = true;
                    CurrentCharacter = letterindex;
                    OriginalColor = texts[CurrentCharacter].color;
                    texts[CurrentCharacter].color = SelectedColor;
                }

            }


        }
    }

    public string GetName()
    {
        return "" + CurrentName;
    }

    public void SendName()
    {
        if(ScoreManager.manager)
        {
            string result = new string(CurrentName);
            ScoreManager.manager.ChangeName(result);
        }
    }
}
