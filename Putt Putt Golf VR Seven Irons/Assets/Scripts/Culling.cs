﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Culling : MonoBehaviour
{
    public MeshRenderer m_renderer;
    public Camera cam;
    //public Renderer r;

        
   
    private void Update()
    { 
        if (m_renderer != null) //if it has a renderer
        {
            if (m_renderer.IsVisibleFrom(cam)) //if its visable from the camera
            {
                 m_renderer.enabled = true; //turn renderer on
               // Debug.Log("Vis");
            }
            else
            {
                //Debug.Log("NotVis");
                 m_renderer.enabled = false; //turn renderer off
            }
        }      
    }
   

}
